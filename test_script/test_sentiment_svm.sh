#!/bin/bash
cd ..
for i in 0 1 2 3 4 5 6 7 8 9
do
	echo "round $i"
	rm -rf SENTIMENT_training
	rm -rf SENTIMENT_dev
	tar -xzf SENTIMENT_training.tar.gz
	mkdir SENTIMENT_dev
	./preprocess.py --mode 4 --path SENTIMENT_training SENTIMENT_dev
	./preprocess.py --mode 3 --path SENTIMENT_dev tmp/sentiment_test_result.txt


	./preprocess.py --mode 1 --option 0 --path SENTIMENT_training  part2/sentiment_svm_training.txt SENTIMENT_dev part2/sentiment_svm_test.txt
	cd svm_light_osx.8.4_i7/
	./svm_learn ../part2/sentiment_svm_training.txt ../part2/sentiment.svm.model >NUL 
	./svm_classify ../part2/sentiment_svm_test.txt ../part2/sentiment.svm.model ../part2/sentiment.svm.raw.out>NUL
	cd ..
	./postprocess.py --mode 1 --label NEG POS --path part2/sentiment.svm.raw.out part2/sentiment.svm.out
	./postprocess.py --mode 0 --label NEG POS --path part2/sentiment.svm.out tmp/sentiment_test_result.txt

	./preprocess.py --mode 1 --option 6 --path SENTIMENT_training  part2/sentiment_svm_training.txt SENTIMENT_dev part2/sentiment_svm_test.txt
	cd svm_light_osx.8.4_i7/
	./svm_learn ../part2/sentiment_svm_training.txt ../part2/sentiment.svm.model >NUL 
	./svm_classify ../part2/sentiment_svm_test.txt ../part2/sentiment.svm.model ../part2/sentiment.svm.raw.out>NUL
	cd ..
	./postprocess.py --mode 1 --label NEG POS --path part2/sentiment.svm.raw.out part2/sentiment.svm.out
	./postprocess.py --mode 0 --label NEG POS --path part2/sentiment.svm.out tmp/sentiment_test_result.txt

done