#!/bin/bash
cd ..
for i in 0 1 2 3 4 5 6 7 8 9
do
	echo "round $i"
	rm -rf SPAM_dev
	rm -rf SPAM_training
	tar -xzf SPAM_training.tar.gz
	mkdir SPAM_dev
	./preprocess.py --mode 4 --path SPAM_training SPAM_dev
	./preprocess.py --mode 3 --path SPAM_dev tmp/spam_test_result.txt


	./preprocess.py --mode 1 --option 1 --path SPAM_training part2/spam_svm_training.txt SPAM_dev part2/spam_svm_test.txt
	cd svm_light_osx.8.4_i7/
	./svm_learn ../part2/spam_svm_training.txt ../part2/spam.svm.model>NUL 
	./svm_classify ../part2/spam_svm_test.txt ../part2/spam.svm.model ../part2/spam.svm.raw.out>NUL
	cd ..
	./postprocess.py --mode 1 --label HAM SPAM --path part2/spam.svm.raw.out part2/spam.svm.out
	./postprocess.py --mode 0 --label HAM SPAM --path part2/spam.svm.out tmp/spam_test_result.txt 


	./preprocess.py --mode 1 --option 2 --path SPAM_training part2/spam_svm_training.txt SPAM_dev part2/spam_svm_test.txt
	cd svm_light_osx.8.4_i7/
	./svm_learn ../part2/spam_svm_training.txt ../part2/spam.svm.model>NUL
	./svm_classify ../part2/spam_svm_test.txt ../part2/spam.svm.model ../part2/spam.svm.raw.out>NUL
	cd ..
	./postprocess.py --mode 1 --label HAM SPAM --path part2/spam.svm.raw.out part2/spam.svm.out
	./postprocess.py --mode 0 --label HAM SPAM --path part2/spam.svm.out tmp/spam_test_result.txt 
done