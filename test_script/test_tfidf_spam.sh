#!/bin/bash
cd ..
for i in 0 1 2 3 4 5 6 7 8 9
do
	echo "round $i"
	rm -rf SPAM_dev
	rm -rf SPAM_training
	tar -xzf SPAM_training.tar.gz
	mkdir SPAM_dev
	./preprocess.py --mode 4 --path SPAM_training SPAM_dev
	./preprocess.py --mode 3 --path SPAM_dev tmp/spam_test_result.txt
	./preprocess.py --mode 0 --option 1 --path SPAM_training spam_training.txt SPAM_dev spam_test.txt
	./nblearn.py spam_training.txt spam.nb
	./nbclassify.py spam.nb spam_test.txt > spam.out
	./postprocess.py --mode 0 --label HAM SPAM --path spam.out tmp/spam_test_result.txt 
	./preprocess.py --mode 0 --option 3 --path SPAM_training spam_training.txt SPAM_dev spam_test.txt
	./nblearn.py spam_training.txt spam.nb
	./nbclassify.py spam.nb spam_test.txt > spam.out
	./postprocess.py --mode 0 --label HAM SPAM --path spam.out tmp/spam_test_result.txt 
done