#!/bin/bash
cd ..
for i in 0 1 2 3 4
do
	echo "round $i"
	rm -rf SENTIMENT_training
	rm -rf SENTIMENT_dev
	tar -xzf SENTIMENT_training.tar.gz
	mkdir SENTIMENT_dev
	./preprocess.py --mode 4 --path SENTIMENT_training SENTIMENT_dev
	./preprocess.py --mode 3 --path SENTIMENT_dev tmp/sentiment_test_result.txt


	./preprocess.py --mode 2 --option 0 --path SENTIMENT_training  part2/sentiment_megam_training.txt SENTIMENT_dev part2/sentiment_megam_test.txt
	cd megam_0.92/
	./megam -nc  multiclass ../part2/sentiment_megam_training.txt > ../part2/sentiment.megam.model  
	./megam -nc  -predict ../part2/sentiment.megam.model multiclass ../part2/sentiment_megam_test.txt > ../part2/sentiment.megam.raw.out
	cd ..
	./postprocess.py --mode 2 --label NEG POS --path part2/sentiment.megam.raw.out part2/sentiment.megam.out
	./postprocess.py --mode 0 --label NEG POS --path part2/sentiment.megam.out tmp/sentiment_test_result.txt

	./preprocess.py --mode 2 --option 6 --path SENTIMENT_training  part2/sentiment_megam_training.txt SENTIMENT_dev part2/sentiment_megam_test.txt
	cd megam_0.92/
	./megam -nc  multiclass ../part2/sentiment_megam_training.txt > ../part2/sentiment.megam.model  
	./megam -nc  -predict ../part2/sentiment.megam.model multiclass ../part2/sentiment_megam_test.txt > ../part2/sentiment.megam.raw.out
	cd ..
	./postprocess.py --mode 2 --label NEG POS --path part2/sentiment.megam.raw.out part2/sentiment.megam.out
	./postprocess.py --mode 0 --label NEG POS --path part2/sentiment.megam.out tmp/sentiment_test_result.txt

done