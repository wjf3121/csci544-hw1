#!/bin/bash
cd ..
for i in 0 1 2 3 4 5 6 7 8 9
do
	echo "round $i"
	rm -rf SPAM_dev
	rm -rf SPAM_training
	tar -xzf SPAM_training.tar.gz
	mkdir SPAM_dev
	./preprocess.py --mode 4 --path SPAM_training SPAM_dev
	./preprocess.py --mode 3 --path SPAM_dev tmp/spam_test_result.txt


	./preprocess.py --mode 2 --option 0 --path SPAM_training part2/spam_megam_training.txt SPAM_dev part2/spam_megam_test.txt
	cd megam_0.92/
	./megam -nc  multiclass ../part2/spam_megam_training.txt > ../part2/spam.megam.model
	./megam -nc  -predict ../part2/spam.megam.model multiclass ../part2/spam_megam_test.txt > ../part2/spam.megam.raw.out
	cd ..
	./postprocess.py --mode 2 --label HAM SPAM --path part2/spam.megam.raw.out part2/spam.megam.out
	./postprocess.py --mode 0 --label HAM SPAM --path part2/spam.megam.out tmp/spam_test_result.txt 


	./preprocess.py --mode 2 --option 1 --path SPAM_training part2/spam_megam_training.txt SPAM_dev part2/spam_megam_test.txt
	cd megam_0.92/
	./megam -nc  multiclass ../part2/spam_megam_training.txt > ../part2/spam.megam.model
	./megam -nc  -predict ../part2/spam.megam.model multiclass ../part2/spam_megam_test.txt > ../part2/spam.megam.raw.out
	cd ..
	./postprocess.py --mode 2 --label HAM SPAM --path part2/spam.megam.raw.out part2/spam.megam.out
	./postprocess.py --mode 0 --label HAM SPAM --path part2/spam.megam.out tmp/spam_test_result.txt 
done