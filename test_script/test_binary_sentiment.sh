#!/bin/bash
cd ..
for i in 0 1 2 3 4 5 6 7 8 9
do
	echo "round $i"
	rm -rf SENTIMENT_training
	rm -rf SENTIMENT_dev
	tar -xzf SENTIMENT_training.tar.gz
	mkdir SENTIMENT_dev
	./preprocess.py --mode 4 --path SENTIMENT_training SENTIMENT_dev
	./preprocess.py --mode 3 --path SENTIMENT_dev tmp/sentiment_test_result.txt
	./preprocess.py --mode 0 --option 6 --path SENTIMENT_training  sentiment_training.txt SENTIMENT_dev sentiment_test.txt
	./nblearn.py sentiment_training.txt sentiment.nb
	./nbclassify.py sentiment.nb sentiment_test.txt > sentiment.out
	./postprocess.py --mode 0 --label NEG POS --path sentiment.out tmp/sentiment_test_result.txt
	./preprocess.py --binary --mode 0 --option 6 --path SENTIMENT_training  sentiment_training.txt SENTIMENT_dev sentiment_test.txt
	./nblearn.py sentiment_training.txt sentiment.nb
	./nbclassify.py sentiment.nb sentiment_test.txt > sentiment.out
	./postprocess.py --mode 0 --label NEG POS --path sentiment.out tmp/sentiment_test_result.txt
done