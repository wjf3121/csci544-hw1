#!/usr/bin/env python3
from collections import defaultdict, OrderedDict
import os
import re
import argparse
import operator
import shutil
import random

DATA_SET_DIR = './SPAM_training'

TRAINING_OUTPUT_PATH = './spam_training.txt'

TEST_SET_DIR = './SPAM_dev'

TEST_OUTPUT_PATH = './spam_test.txt'

TEST_RESULT_PATH = './spam_test_result.txt'

NEGATE = set(['not', 'non', 'no', 'neither', 'nor'])

def pick_test_files(training_path, test_path):
	label_file_dict = defaultdict(list)
	for fn in sorted(os.listdir(training_path)):
		label = (fn.split('.')[0])
		label_file_dict[label].append(fn)
	for label in label_file_dict:
		file_indexes = random.sample(range(0, len(label_file_dict[label])), len(label_file_dict[label])//10)
		for index in file_indexes:
			src = os.path.join(training_path, label_file_dict[label][index])
			dst = test_path
			shutil.move(src, dst)

def get_word_dict(dir_path, process_feature = lambda x: x.split()):
	word_dict, label_words_dict = defaultdict(int), defaultdict(lambda : defaultdict(int))
	label_set = []
	for fn in sorted(os.listdir(dir_path)):
		file_path = os.path.join(dir_path, fn)
		label = (fn.split('.')[0])
		if label not in label_set: 
			label_set.append(label)
		if os.path.isfile(file_path):
			with open(file_path, 'r', encoding='utf-8', errors='ignore') as input_file:
				for line in input_file:
					for word in process_feature(line):
						word_dict[word] += 1
						label_words_dict[label][word] += 1
	return word_dict, label_words_dict, label_set

def generate_feature_id(word_dict):
	id_dict = {}
	for i,key in enumerate(word_dict):
		id_dict[key] = i + 1
	return id_dict

def get_frequent_words(label_words_dict):
	sorted_label_words = {}
	for label in label_words_dict:
		sorted_label_words[label] = sorted(label_words_dict[label].items(), key=operator.itemgetter(1), reverse=True)
	label_top_words = [set(sorted_label_words[label][-i][0] for i in range(len(sorted_label_words[label])//50)) for label in sorted_label_words]
	return set.intersection(*label_top_words)

def get_rare_words(label_words_dict):
	sorted_label_words = {}
	for label in label_words_dict:
		sorted_label_words[label] = sorted(label_words_dict[label].items(), key=operator.itemgetter(1), reverse=True)
	label_top_words = [set(i[0] for i in sorted_label_words[label] if i[1] == 1) for label in sorted_label_words]
	return set.union(*label_top_words)


def dumb_feature_tfidf(line):
	new_feature = line.lower().split()
	return [x+'_tfidf' for x in new_feature]

def alphanumeric(line):
	return (re.sub('[^0-9a-zA-Z\']+', ' ', line).lower()).split()

def alphanumeric_tfidf(line):
	new_feature = (re.sub('[^0-9a-zA-Z\']+', ' ', line).lower()).split()
	return [x+'_tfidf' for x in new_feature]

def alpha_only(line):
	return (re.sub('[^a-zA-Z]+', ' ', line).lower()).split()

def alpha_with_stops(stop_words):
	def f(line):
		words = (re.sub('[^a-zA-Z]+', ' ', line).lower()).split()
		return [w for w in words if w not in stop_words]
	return f

def alphafloat_with_stops(stop_words):
	def f(line):
		float_regex=r'[-+]?[0-9]*\.?[0-9]+(?:[eE][-+]?[0-9]+)?'
		numbers = re.findall(float_regex,line)
		words = (re.sub('[^a-zA-Z]+', ' ', line).lower()).split()
		return [w for w in words + numbers if w not in stop_words]
	return f


def alphanumeric_with_stops(stop_words):
	def f(line):
		words = (re.sub('[^0-9a-zA-Z]+', ' ', line).lower()).split()
		return [w for w in words if w not in stop_words]
	return f

def alphanumeric_with_negate_tag(line):
	new_feature = []
	words = (re.sub('[^0-9a-zA-Z\']+', ' ', line).lower()).split()
	prev = ''
	for w in words:
		if prev in NEGATE or prev.endswith('n\'t'):
			new_feature.append('neg_'+w)
		else:
			new_feature.append(w)
		prev = w
	return new_feature

def alphanumeric_ngram_with_rare_words(ns, rare_words):
	def f(line):
		new_feature = []
		words = (re.sub('[^0-9a-zA-Z\']+', ' ', line).lower()).split()
		for n in ns:
			for i in range(len(words)-n+1):
				if '_'.join(words[i:i+n]) not in rare_words:
					new_feature.append('_'.join(words[i:i+n]))
		return new_feature
	return f

def alphanumeric_ngram(ns):
	def f(line):
		new_feature = []
		words = (re.sub('[^0-9a-zA-Z\']+', ' ', line).lower()).split()
		for n in ns:
			for i in range(len(words)-n+1):
				new_feature.append('_'.join(words[i:i+n]))
		return new_feature
	return f

def generate_features(dir_path, training=True, process_feature = lambda x: x.split(), binary = False):
	features = []
	for fn in sorted(os.listdir(dir_path)):
		file_path = os.path.join(dir_path, fn)
		label = (fn.split('.')[0])
		if os.path.isfile(file_path):
			with open(file_path, 'r', encoding='utf-8', errors='ignore') as input_file:
				feature = [label] if training else []
				text = ''
				for line in input_file:
					text += ' '+line.rstrip()
				if binary:
					feature.extend(set(process_feature(text)))
				else:
					feature.extend(process_feature(text))
				features.append(feature)
	return features


def write_features_to_file(features, output_path, training=True, format = lambda _ : _):
	with open(output_path, 'w') as output_file:
		for feature in features:
			output_file.write(' '.join(format(feature)) + '\n')

def generate_real_test_result(dir_path, output_path):
	with open(output_path, 'w') as output_file:
		for fn in sorted(os.listdir(dir_path)):
			file_path = os.path.join(dir_path, fn)
			if os.path.isfile(file_path):
				label = (fn.split('.')[0])
				output_file.write(label + '\n')


def svm_feature_format(label_set, id_dict):
	def f(feature):
		id_count_dict = defaultdict(int)

		for f in feature[1:]:
			if f not in id_dict:
				id_dict[f] = len(id_dict) + 1
			id_count_dict[id_dict[f]] += 1

		ordered_id_dict = OrderedDict(sorted(id_count_dict.items()))
		new_feature = ['+1'] if feature[0] == label_set[0] else ['-1']

		for key in ordered_id_dict:
			new_feature.append(str(key) + ':' + str(id_count_dict[key]))
		return new_feature

	return f

def megam_feature_format(id_dict, fval = False):
	def f(feature):
		'''
		word_count_dict = defaultdict(int)

		for f in feature[1:]:
			word_count_dict[f] += 1

		ordered_word_count_dict = OrderedDict(sorted(word_count_dict.items()))
		new_feature = [feature[0]]

		for key in ordered_word_count_dict:
			if not fval:
				new_feature.append(str(key))
			else:
				new_feature.append(str(key) + ' ' + str(word_count_dict[key]))
		return new_feature
		'''
		word_count_dict = defaultdict(int)

		for f in feature[1:]:
			word_count_dict[f] += 1

		ordered_word_count_dict = OrderedDict(sorted(word_count_dict.items()))
		for f in feature[1:]:
			if f not in id_dict:
				id_dict[f] = len(id_dict) + 1
			word_count_dict[f] += 1

		new_feature = [feature[0]]

		for key in ordered_word_count_dict:
			new_feature.append('F'+str(id_dict[key]))
		return new_feature
	return f

def main():
	
	parser = argparse.ArgumentParser()
	parser.add_argument("--binary", help="binary", default = False,
                    action="store_true")
	parser.add_argument('--mode',
						type=int,
						choices=[0, 1, 2, 3, 4],
						required=True, 
						help='Select feature style, 0 for NB , 1 for SVM, 2 for MegaM, \
						3 for generating correct results, 4 for pick dev test files')
	parser.add_argument('--option',
						type=int,
						choices=[0, 1, 2, 3, 4, 5, 6],
						default = 0,
						help='Select feature processing mode, 0 for plain feature, 1 for alphanumeric only, \
						2 for alphanumeric with stop words')
	parser.add_argument('--path',
						type=str,
						nargs='+',
						help='List of paths in order of TRAIN_DATA_SET_DIR, TRAIN_OUTPUT_PATH, TEST_DATA_SET_DIR, TEST_OUTPUT_PATH')
	args = parser.parse_args()



	word_dict_processors = [lambda x: x.lower().split(), alphanumeric, alphanumeric, alphanumeric,
			dumb_feature_tfidf, alphanumeric_ngram([2,3]), alphanumeric_ngram([2,3])]

	word_dict, label_words_dict, label_set = get_word_dict(args.path[0], 
		process_feature = word_dict_processors[args.option])

	frequent_words = get_frequent_words(label_words_dict)
	rare_words = get_rare_words(label_words_dict)
	stop_words = set.union(frequent_words, rare_words)
	id_dict = generate_feature_id(word_dict)

	feature_processors = [lambda x: x.lower().split(), alphanumeric, alphanumeric_with_stops(stop_words), 
		alphanumeric_tfidf, dumb_feature_tfidf, 
		alphanumeric_ngram([2, 3]), alphanumeric_ngram_with_rare_words([2,3], rare_words)]
	if args.mode == 0:
		train_feature = generate_features(args.path[0], process_feature = feature_processors[args.option], binary = args.binary)
		write_features_to_file(train_feature, args.path[1])

		test_feature = generate_features(args.path[2], training = False, process_feature = feature_processors[args.option], binary = args.binary)
		write_features_to_file(test_feature, args.path[3], training = False)

	if args.mode == 1:
		train_feature = generate_features(args.path[0], process_feature = feature_processors[args.option], binary = args.binary)
		write_features_to_file(train_feature, args.path[1], format = svm_feature_format(label_set, id_dict))

		test_feature = generate_features(args.path[2], process_feature = feature_processors[args.option], binary = args.binary)
		write_features_to_file(test_feature, args.path[3], format = svm_feature_format(label_set, id_dict))

	if args.mode == 2:
		train_feature = generate_features(args.path[0], process_feature = feature_processors[args.option], binary = args.binary)
		write_features_to_file(train_feature, args.path[1], format = megam_feature_format(id_dict))

		test_feature = generate_features(args.path[2], process_feature = feature_processors[args.option], binary = args.binary)
		write_features_to_file(test_feature, args.path[3], format = megam_feature_format(id_dict))

	if args.mode == 3: 
		generate_real_test_result(args.path[0], args.path[1])

	if args.mode == 4: 
		pick_test_files(args.path[0], args.path[1])

if __name__ == '__main__':
	main()
