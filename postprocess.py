#!/usr/bin/env python3
import argparse


def read_labels(correct_path):
    labels = []
    with open(correct_path, 'r') as corrent_file:
        for line in corrent_file:
            if line.rstrip() not in labels:
                labels.append(line.rstrip())
    return labels


def format_svm_result(result, labels):
    val = float(result.rstrip())
    return labels[0] if val > 0 else labels[1]


def format_megam_result(result, labels):
    return result.split()[0]


def format_result(input_path, output_path, labels, formatter):
    with open(input_path, 'r') as input_file:
        with open(output_path, 'w') as output_file:
            for line in input_file:
                output_file.write(formatter(line, labels) + '\n')
    return


def process_f_score(predict_path, correct_path, label):
    predicts, corrects = [], []
    with open(predict_path, 'r') as predict_file:
        for line in predict_file:
            predicts.append(line.rstrip())
    with open(correct_path, 'r') as corrent_file:
        for line in corrent_file:
            corrects.append(line.rstrip())
    return calc_f_score(predicts, corrects, label)


# http://en.wikipedia.org/wiki/Precision_and_recall
def calc_precision_recall(predicts, corrects, label):
    retrieved_labels, relevant_labels, correct_labels = 0, 0, 0
    for predict, correct in zip(predicts, corrects):
        if predict == label:
            retrieved_labels += 1
        if correct == label:
            relevant_labels += 1
        if correct == label and predict == label:
            correct_labels += 1
    return correct_labels / retrieved_labels, correct_labels / relevant_labels


def calc_f_score(predicts, corrects, label):
    precision, recall = calc_precision_recall(predicts, corrects, label)
    return precision, recall, 2 * precision * recall / (precision + recall)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--mode',
                        type=int,
                        choices=[0, 1, 2],
                        required=True,
                        help='Select mode, 0 for evaluation, 1 for formatting SVM output, 2 for formatting MegaM output')
    parser.add_argument('--label',
                        type=str,
                        nargs=2,
                        required=True,
                        help='Name of label')
    parser.add_argument('--path',
                        type=str,
                        nargs='+',
                        help='List of paths in order of INPUT_PATH, COMPARE_PATH, OUTPUT_PATH')

    args = parser.parse_args()

    if args.mode == 0:
        print(process_f_score(args.path[0], args.path[1], args.label[0]))
        print(process_f_score(args.path[0], args.path[1], args.label[1]))

    if args.mode == 1:
        format_result(args.path[0], args.path[1], args.label, format_svm_result)

    if args.mode == 2:
        format_result(args.path[0], args.path[1], args.label, format_megam_result)


if __name__ == '__main__':
    main()
